    <div id="body2main" class="sc-clearfix">

      <figure id="body2image"></figure>
      <h1 id="body2title">Get the app</h1>
      <p id="body2paragraphe">Never stop listening. Take your playlists and likes wherever you go.</p>
      <a id="body2AppStore" target="_blank" href="https://itunes.apple.com/en/app/soundcloud/id336353151?mt=8"> Download on the App Store </a>
      <a id="body2PlayStore" target="_blank" href="https://play.google.com/store/apps/details?id=com.soundcloud.android"> Get it on Google Play </a>
      <span id="body2Liaison"> or <a id="body2Petitlien" href="https://soundcloud.com/mobile">Learn more</a></span>

    </div>
