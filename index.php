<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
		<link rel="stylesheet" href="header.css"/>
		<link rel="stylesheet" href="body1.css"/>
		<link rel="stylesheet" href="body2.css"/>
		<link rel="stylesheet" href="body3.css"/>
		<link rel="stylesheet" href="footer1.css"/>
		<link rel="stylesheet" href="footer2.css"/>
		<link rel="stylesheet" href="index.css"/>
</head>

<body>
		<?php
	include "header.php";
	include "body1.php";
	include "body2.php";
	include "body3.php";
	include "footer1.php";
	include "footer2.php";
	?>
</body>
</html>
