<header>

  <div id="titre">
    <img src="cloud-e365a4.png"/>
    <h1>SOUND-CLOUD</h1>
  </div>

  <div id="login">
    <div id="sign_in"><a href="#">identifies-toi</a></div>
    <div id="create_account"><a href="#">Crées un compte</a></div>
  </div>

  <h2>
    Trouves la musique que tu aimes. Découvres de nouveaux morceaux.</br>
    Connectes toi à tes artistes favoris.
  </h2>

  <div id="form">
    <input type="text" placeholder="Trouves des artistes, bandes, morceaux, podcasts" />
    <p>ou</p>
    <div id="upload"><a href="#">Charges le tiens</a></div>
  </div>

</header>
